package com.estefanyangeles.rest.utils;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("app")

public class Configuracion {

    private String titulo;
    private String autor;
    private String modo;


    public Configuracion(String titulo, String autor, String modo) {
        this.titulo = titulo;
        this.autor = autor;
        this.modo = modo;
    }

    public Configuracion() {
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getModo() {
        return modo;
    }

    public void setModo(String modo) {
        this.modo = modo;
    }


}
